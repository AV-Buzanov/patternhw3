package ru.iteco.patterns.builder.state;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * EmailBuilder.
 *
 * @author Aleksey Buzanov
 */
public class EmailBuilder {
    private static final String DEFAULT_FROM = "Неизвестный отправитель";

    public IEmailFromOrFirstToBuilder subject(String subject) {
        return new EmailFromOrFirstToBuilder(subject);
    }

    private class EmailFromOrFirstToBuilder implements IEmailFromOrFirstToBuilder {
        private String subject;

        public EmailFromOrFirstToBuilder(String subject) {
            this.subject = subject;
        }

        @Override
        public IEmailOtherToAndCopyBuilder to(String to) {
            return new EmailOtherToAndCopyBuilder(subject, DEFAULT_FROM, Collections.singletonList(to));
        }

        @Override
        public IEmailOtherToAndCopyBuilder toAll(List<String> toAll) {
            return new EmailOtherToAndCopyBuilder(subject, DEFAULT_FROM, toAll);
        }

        @Override
        public IEmailOtherToAndCopyBuilder toAll(String... toAll) {
            return new EmailOtherToAndCopyBuilder(subject, DEFAULT_FROM, Arrays.asList(toAll));
        }

        @Override
        public IEmailFirstToBuilder from(String from) {
            return new EmailFirstToBuilder(subject, from);
        }
    }

    private class EmailFirstToBuilder implements IEmailFirstToBuilder {
        private String subject;
        private String from;

        public EmailFirstToBuilder(String subject, String from) {
            this.subject = subject;
            this.from = from;
        }

        @Override
        public IEmailOtherToAndCopyBuilder to(String to) {
            return new EmailOtherToAndCopyBuilder(subject, from, Collections.singletonList(to));
        }

        @Override
        public IEmailOtherToAndCopyBuilder toAll(List<String> toAll) {
            return new EmailOtherToAndCopyBuilder(subject, from, toAll);
        }

        @Override
        public IEmailOtherToAndCopyBuilder toAll(String... toAll) {
            return new EmailOtherToAndCopyBuilder(subject, from, Arrays.asList(toAll));
        }
    }

    private class EmailOtherToAndCopyBuilder implements IEmailOtherToAndCopyBuilder {
        private String subject;
        private String from;
        private final List<String> to = new ArrayList<>();

        public EmailOtherToAndCopyBuilder(String subject, String from, List<String> to) {
            this.subject = subject;
            this.from = from;
            this.to.addAll(to);
        }

        @Override
        public IEmailOtherToAndCopyBuilder to(String to) {
            this.to.add(to);
            return this;
        }

        @Override
        public IEmailOtherToAndCopyBuilder toAll(List<String> toAll) {
            this.to.addAll(toAll);
            return this;
        }

        @Override
        public IEmailOtherCopyToAndContentBuilder copyTo(String copyTo) {
            return new EmailOtherCopyToAndContentBuilder(subject, from, to, Arrays.asList(copyTo));
        }

        @Override
        public IEmailOtherCopyToAndContentBuilder copyToAll(List<String> copyToAll) {
            return new EmailOtherCopyToAndContentBuilder(subject, from, to, copyToAll);
        }

        @Override
        public IEmailOtherCopyToAndContentBuilder copyToAll(String... copyToAll) {
            return new EmailOtherCopyToAndContentBuilder(subject, from, to, Arrays.asList(copyToAll));
        }
    }

    private class EmailOtherCopyToAndContentBuilder implements IEmailOtherCopyToAndContentBuilder {
        private String subject;
        private String from;
        private final List<String> to = new ArrayList<>();
        private final List<String> copyTo = new ArrayList<>();

        public EmailOtherCopyToAndContentBuilder(String subject, String from, List<String> to, List<String> copyTo) {
            this.subject = subject;
            this.from = from;
            this.to.addAll(to);
            this.copyTo.addAll(copyTo);
        }

        @Override
        public IEmailOtherCopyToAndContentBuilder copyTo(String copyTo) {
            this.copyTo.add(copyTo);
            return this;
        }

        @Override
        public IEmailOtherCopyToAndContentBuilder copyToAll(List<String> copyToAll) {
            this.copyTo.addAll(copyToAll);
            return this;
        }

        @Override
        public IEmailOtherCopyToAndContentBuilder copyToAll(String... copyToAll) {
            this.copyTo.addAll(Arrays.asList(copyToAll));
            return this;
        }

        @Override
        public IFinalEmailBuilder content(Content content) {
            return new FinalEmailBuilder(subject, from, to, copyTo, content);
        }
    }

    private class FinalEmailBuilder implements IFinalEmailBuilder {
        private String subject;
        private String from;
        private List<String> to;
        private List<String> copy;
        private Content content;

        public FinalEmailBuilder(String subject, String from, List<String> to, List<String> copy, Content content) {
            this.subject = subject;
            this.from = from;
            this.to = to;
            this.copy = copy;
            this.content = content;
        }

        @Override
        public String build() {
            String joinedTo = to.stream()
                    .distinct()
                    .collect(Collectors.joining(";"));
            String joinedCopy = copy.stream()
                    .filter(s -> !to.contains(s))
                    .collect(Collectors.joining(";"));
            return String.format("Email \n" +
                            "from: %s \n" +
                            "to: %s \n" +
                            "copy: %s \n" +
                            "subject: %s \n" +
                            "%s \n" +
                            "%s",
                    from, joinedTo, joinedCopy, subject, content.getBody(), content.getSignature());
        }
    }

    public interface IEmailFromOrFirstToBuilder {
        IEmailOtherToAndCopyBuilder to(String to);

        IEmailOtherToAndCopyBuilder toAll(List<String> toAll);

        IEmailOtherToAndCopyBuilder toAll(String... toAll);

        IEmailFirstToBuilder from(String from);
    }

    public interface IEmailFirstToBuilder {
        IEmailOtherToAndCopyBuilder to(String to);

        IEmailOtherToAndCopyBuilder toAll(List<String> toAll);

        IEmailOtherToAndCopyBuilder toAll(String... toAll);
    }

    public interface IEmailOtherToAndCopyBuilder {
        IEmailOtherToAndCopyBuilder to(String to);

        IEmailOtherToAndCopyBuilder toAll(List<String> toAll);

        IEmailOtherCopyToAndContentBuilder copyTo(String copyTo);

        IEmailOtherCopyToAndContentBuilder copyToAll(List<String> copyToAll);

        IEmailOtherCopyToAndContentBuilder copyToAll(String... copyToAll);

    }

    public interface IEmailOtherCopyToAndContentBuilder {
        IEmailOtherCopyToAndContentBuilder copyTo(String copyTo);

        IEmailOtherCopyToAndContentBuilder copyToAll(List<String> copyToAll);

        IEmailOtherCopyToAndContentBuilder copyToAll(String... copyToAll);

        IFinalEmailBuilder content(Content content);
    }

    public interface IFinalEmailBuilder {
        String build();
    }
}


