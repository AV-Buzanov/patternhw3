package ru.iteco.patterns.builder.state;

/**
 * Content.
 *
 * @author Aleksey Buzanov
 */
public class Content {
    private String body;
    private String signature;

    private Content(ContentBuilder builder) {
        this.body = builder.body;
        this.signature = builder.signature;
    }

    public static ContentBuilder builder() {
        return new ContentBuilder();
    }

    public static class ContentBuilder {
        private String body;
        private String signature;

        public ContentBuilder withBody(String body) {
            this.body = body;
            return this;
        }

        public ContentBuilder withSignature(String signature) {
            this.signature = signature;
            return this;
        }

        public Content build() {
            return new Content(this);
        }
    }

    public String getBody() {
        return body;
    }

    public String getSignature() {
        return signature;
    }
}
