package ru.iteco.patterns.builder.state;

/**
 * Main.
 *
 * @author Aleksey Buzanov
 */
public class Main {

    public static void main(String[] args) {
        EmailBuilder emailBuilder = new EmailBuilder();
        String letter = emailBuilder
                .subject("Тема письма")
                .from("Автор письма")
                .to("Получатель письма")
                .to("Получатель письма")
                .to("Второй получатель")
                .copyTo("Получатель копии")
                .copyTo("Второй получатель копии")
                .copyTo("Получатель письма")
                .content(Content.builder().withBody("Тело письма").withSignature("Подпись").build())
                .build();
        System.out.println(letter);
    }

}
